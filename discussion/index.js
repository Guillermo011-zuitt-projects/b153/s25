// import Node's built-in HTTP module, a set of tools that can handle server requests and responses, etc.
let http = require("http")

// the createServer() method lets you create an HTTP server that listens to requests on specified port and can alo send responses back to the client
// user sends a request to view a page on port 4000 -> createServer() reacts to this request by executing our code inside of the function inside of it, which sends the request
const server = http.createServer(function(request, response){
	// use writeHead() method to set a status code for the response 
	// 200 means OK
	// set the content-type of the response as a plain text message
	response.writeHead(200, {'Content-Type': 'text/plain'})

	response.end("Hello me!")
})

const port = 4000
/*
0-1023 - well-known ports are kept reserver/never used because they are typically used for connections important for the system
1024-65535 - can be used
if an application is already running on a port (ex. 4000), no other applications can use the same port since it's busy
*/


server.listen(port) //if a user tries to access port 4000, send the response that we added in createServer

// show confirmation message that the server is running on the assigned port
console.log(`Server running at port ${port}`)

/*
Terminology notes
URL: localhost



*/