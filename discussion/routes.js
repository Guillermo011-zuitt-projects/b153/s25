let http = require("http")

const server = http.createServer(function(request, response){
	if(request.url === '/greeting'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Hello to you!")
	}
	else if(request.url === '/'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("This is the homepage.")
	}else{
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end("Page not Found")		
	}

})

const port = 4000

server.listen(port)

console.log(`Server running at port ${port}`)